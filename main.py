from flask import Flask, session, redirect, url_for, request, render_template
from logging import exception
import mysql.connector

app = Flask(__name__)

# Set secret key for User Session.
app.secret_key = 'agfertgdhj478fershhy23lkj#fg'


# Function to connect to Mysql Database.
def connectToDb():
    connect_db = mysql.connector.connect(
        host="localhost",
        user="root",
        password="root",
        database="olms_flask"
    )
    return connect_db


# Function to get all available courses.
def getAllAvailableCourses():
    # Connect to Database
    connect_db = connectToDb()
    cursor = connect_db.cursor()
    try:
        query = 'select course_id, course_name from lecture_classes'
        cursor.execute(query)
        result = cursor.fetchall()
        cursor.close()
        return result
    except exception as e:
        print(e)
    finally:
        cursor.close()


# Function to get all registered courses by learner.
def getAllRegisteredCourses():
    student_id = session.get('student_id')
    # Connect to Database
    connect_db = connectToDb()
    cursor = connect_db.cursor()
    try:
        query = "select lec.enrollment_id, lc.course_name, ld.name from learner_enrolled_courses as lec left join lecture_classes as lc on " \
                "lc.course_id = lec.course_id left join lecture_details as ld on ld.lecture_id=lc.lecture_id where " \
                "lec.student_id = %s"
        var = (student_id,)
        cursor.execute(query, var)
        result = cursor.fetchall()
        return result
    except exception as e:
        print(e)
    finally:
        cursor.close()


# Unenroll course.
@app.route('/unenroll/', methods=['GET', 'POST'])
def unenrollCourse():
    enrollment_id = request.args.get('id', type=int)
    # Connect to Database
    connect_db = connectToDb()
    cursor = connect_db.cursor()
    try:
        query = "delete from learner_enrolled_courses where enrollment_id=%s"
        var = (enrollment_id,)
        cursor.execute(query, var)
        connect_db.commit()
        return redirect(url_for('index'))
    except exception as e:
        print(e)
    finally:
        cursor.close()


# Home Page.
@app.route('/', methods=['GET', 'POST'])
def index():
    if session.get('s_email_pass'):
        course_list = getAllAvailableCourses()
        enrolled_course_list = getAllRegisteredCourses()
        if request.method == 'POST':
            student_id = session.get('student_id')
            course_id = request.form['subject']
            connect_db = connectToDb()
            cursor = connect_db.cursor()
            try:
                # Select all registered courses by learner.
                query1 = "select course_id from learner_enrolled_courses where student_id=%s"
                val = (student_id,)
                cursor.execute(query1, val)
                result = cursor.fetchall()
                count = 0
                for cid in result:
                    if int(course_id) == int(cid[0]):
                        count += 1
                        break
                    else:
                        continue
                if count == 0:
                    query2 = "insert into learner_enrolled_courses (student_id, course_id) values (%s, %s)"
                    val = (student_id, course_id)
                    cursor.execute(query2, val)
                    connect_db.commit()
                    return render_template('index.html', course_list=course_list,
                                           enrolled_course_list=enrolled_course_list, enrolled=True)
                else:
                    return render_template('index.html', course_list=course_list,
                                           enrolled_course_list=enrolled_course_list, already_enrolled=True)
            except mysql.connector.Error as e:
                return render_template('index.html', error=e)
            finally:
                cursor.close()
        else:
            return render_template('index.html', course_list=course_list, enrolled_course_list=enrolled_course_list)
    else:
        return render_template('index.html')


# About Us Page.
@app.route('/about-us/')
def about():
    return render_template('about.html')


# Register Page.
@app.route('/register/', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email'].lower()
        password = request.form['password']
        # Connect to Database
        connect_db = connectToDb()
        cursor = connect_db.cursor()
        try:
            # Select all registered email ids.
            query1 = "select email from learner_details"
            cursor.execute(query1)
            email_result = cursor.fetchall()
            count = 0
            for mail in email_result:
                if email == mail[0]:
                    count += 1
                    break
                else:
                    continue
            if count == 0:
                query2 = "insert into learner_details (name, email, password) values (%s, %s, %s)"
                val = (name, email, password)
                cursor.execute(query2, val)
                connect_db.commit()
                return render_template('register.html', register=True)
            else:
                return render_template('register.html', email_exist=True)
        except mysql.connector.Error as e:
            return render_template('register.html', error=e)
        finally:
            cursor.close()

    # Check if session exist.
    session_exist = session.get('s_email_pass')
    # If user logged in redirect to home page.
    if session_exist:
        return redirect(url_for('index'))
    else:
        return render_template('register.html')


# Login Page.
@app.route('/login/', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form['email'].lower()
        password = request.form['password']
        # Connect to Database
        connect_db = connectToDb()
        cursor = connect_db.cursor()
        try:
            # Check email and password exist in DB.
            query1 = "select student_id, name, email, password from learner_details where email=%s and password = %s"
            email_pass = (email, password)
            cursor.execute(query1, email_pass)
            result = cursor.fetchall()
            if len(result):
                student_id = ''
                name = ''
                for r in result:
                    student_id = r[0]
                    name = r[1]
                session['student_id'] = student_id
                session['s_email_pass'] = str(email) + str(password)
                session['s_name'] = name
                return redirect(url_for('index'))
            else:
                return render_template('login.html', login_fail=True)
        except mysql.connector.Error as e:
            return render_template('login.html', error=e)
        finally:
            cursor.close()
    # Check if session exist.
    session_exist = session.get('s_email_pass')
    # If user logged in redirect to home page.
    if session_exist:
        return redirect(url_for('index'))
    else:
        return render_template('login.html')


# Logout Page.
@app.route('/logout')
def logout():
    session.pop('student_id', None)
    session.pop('s_email_pass', None)
    session.pop('s_name', None)
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True)
